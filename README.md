# Challenge

## Computacenter DE - xPERIENCE, 2023-11-23/24

Dieses Projekt beinhaltet meine Präsentationen als _PaaS_ (_Presentation As A Service_). Die CI/CD-Pipeline wandelt dabei die Markdown-Dateien in dem Verzeichnis `src/` in HTML-Dateien um. Die HTML-Dateien werden dann entweder in einen NGINX-Container verpackt oder als _GitLab-Pages_ veröffentlicht. Der resultierende NGINX-Container wird bei _Google_ oder _AWS_ deployed. Gesteuert wird dies durch die Variable `$INFRA`. Dies Variable kann die Werte

- `aws`
- `google`
- `gitlab`

haben und wählt so das gewünschte Backend (_AWS_, _Google_ oder _GitLab-Pages_) aus.

In dieser Challenge beschränken wir uns auf die Benutzung von GitLab und GitLab-Pages. Sollte _AWS_ oder _Google_ als Ziel verwendet werden, muss die entsprechende Infrastruktur vorab konfiguriert werden. Die jeweiligen README-Dateien unterhalb von `infra/` beschrieben dies im Detail.

**Wenn man sich nun die Pipeline-Definition ansieht, stellt man fest, dass alle Security-Scans auskommentiert sind. In den letzten Läufe der Pipeline finden wir jedoch immer einen Job `kics-iac-sast-0` in der Stage `test`. Warum?**
