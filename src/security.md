---
marp: true
theme: gitlab-theme
title: ##SECURITY_TITLE## - ##SUBTITLE##
footer: 
---

<div style="display:flex; flex-direction:column;">
    <h1 style="margin-top:1em;text-align:right;color:#fc6d27">
      ##SECURITY_TITLE##
    </h1>
    <h2 style="margin-bottom:1.5em;text-align:right;">
      ##SUBTITLE##
    </h2>
    <div style="display:flex; flex-direction:row;">
        <div style="display:flex; flex-direction:row">
            <div>
                <img class="avatar" src="./assets/avatar-chris.jpeg">
            </div>
            <div style="display:flex; flex-direction:column; justify-content:center; margin-left:1em">
                <h2>
                    Christoph Leygraf
                </h2>
                <a href= "mailto:cleygraf@gitlab.com">cleygraf@gitlab.com</a>
            </div>
        </div>
        <div style="display:flex; flex-direction:row; margin-left:2em">
        </div>
    </div>
</div>

<!-- footer: "" -->
---
<!-- header: ""  --> 

![bg 60%](./assets/AI-powered_DevSecOps.png)

---

<div class="white-center"><p>Cars?</p></div>

---

![bg 90%](./assets/motorwagen_1886.jpg)
![bg 90%](./assets/c-class.jpg)

---

![bg 60%](./assets/mb_manufactoring_line.jpg)

---
<!-- header: "" -->

<div class="white-center"><p>And Software?</p></div>

<!-- footer: "" -->
---
<!-- header: "GitLab's Software Factory Approach" -->
<style scoped>
section {
  display: flex;
  flex-direction: column;
  justify-content: start;
  text-align: left;
}
</style>

![bg](./assets/software-factory.svg)

- Ideas go in, applications come out
- Shift security left: checks at each stage
- Full visibility from planning to production

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header: "AI powered - in every step & for every user"  --> 

![bg 90%](./assets/why/ai-gitlab-workflow.png)

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Security Scanning" -->

![bg w:80% vertical](./assets/compliance/securityscan.png)

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

Details: [Secure your application](https://docs.gitlab.com/ee/user/application_security/secure_your_application.html)

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Static Application Security Testing (SAST)" -->

" ... you can use Static Application Security Testing (SAST) to _check your source code for known vulnerabilities_. You can run SAST analyzers in any GitLab tier. The analyzers output JSON-formatted reports as job artifacts.

With GitLab Ultimate, SAST results are also processed so you can:

- See them in merge requests.
- Use them in approval workflows.
- Review them in the security dashboard.

..."

Source: [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/index.html) 

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Static Application Security Testing (SAST)" -->

**Requirements**:

- Runs in `test`stage
- `docker` or `kubernetes` executor

**Configure SAST**:

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - template: Jobs/SAST.gitlab-ci.yml
```

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Dynamic Application Security Testing (DAST)" -->

"If you deploy your web application into a new environment, your application may become exposed to new types of attacks. For example, misconfigurations of your application server or incorrect assumptions about security controls may not be visible from the source code.

Dynamic Application Security Testing (DAST) examines applications for vulnerabilities like these in deployed environments.
..."

Source: [Dynamic Application Security Testing (DAST)](https://docs.gitlab.com/ee/user/application_security/dast/)

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Dynamic Application Security Testing (DAST)" -->

**DAST analyzers**:
- The [DAST proxy-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/proxy-based.html) for scanning traditional applications serving simple HTML.
- The [DAST browser-based analyzer](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html) for scanning applications that make heavy use of JavaScript.
- The [DAST API analyzer](https://docs.gitlab.com/ee/user/application_security/dast_api/index.html) for scanning web APIs.

**Requirements**:

- Runs in `dast`stage
- `docker` executor
- _Target application deployed_

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Dynamic Application Security Testing (DAST)" -->

**Configure DAST(proxy-based analyzer)**:

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - template: DAST.gitlab-ci.yml
```

Provide URL  to be scanned by DAST by using one of these methods::
- `DAST_WEBSITE` CI/CD variable
- `environment_url.txt` file

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Infrastructure as Code scanning" -->

"Infrastructure as Code (IaC) scanning runs in your CI/CD pipeline, _checking your infrastructure definition files for known vulnerabilities_ ...

With GitLab Ultimate, IaC scanning results are also processed so you can:

- See them in merge requests.
- Use them in approval workflows.
- Review them in the vulnerability report.

..."

Source: [Infrastructure as Code scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/index.html)

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Infrastructure as Code scanning" -->

**Requirements**:

- AMD64 architecture. Microsoft Windows is not supported.
- Minimum of 4 GB RAM to ensure consistent performance.
- Runs in `test`stage
- `docker` or `kubernetes` executor

**Configure IaC scanning**:

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - template: Security/SAST-IaC.gitlab-ci.yml
```

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Security Policies" -->

"Policies in GitLab provide _security teams_ a way to require scans of their choice to be run whenever a project pipeline runs according to the configuration specified. Security teams can therefore be _confident that the scans they set up have not been changed, altered, or disabled_ ...

GitLab supports the following security policies:

- [Scan Execution Policy](https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html)
- [Scan Result Policy](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)

..."

Source: [Policies](https://docs.gitlab.com/ee/user/application_security/policies/)

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Security Policies" -->

"All security policies are stored as YAML in a _separate security policy project that gets linked to the development project, group, or sub-group_. This association can be a one-to-many relationship, allowing one security policy project to apply to multiple development projects, groups, or sub-groups ...

All security policies are stored in the `.gitlab/security-policies/policy.yml` YAML file inside the linked security policy project.

..."

Source: [Security policy project](https://docs.gitlab.com/ee/user/application_security/policies/#security-policy-project)

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Artifact signing" -->

- [Streamline security with keyless signing and verification in GitLab](https://about.gitlab.com/blog/2023/09/13/keyless-signing-with-cosign/)
- [Use Sigstore for keyless signing and verification](https://docs.gitlab.com/ee/ci/yaml/signing_examples.html)

![bg right](./assets/security/keylesssigningdiagram.png)

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header: "" -->

[![w:1045px](./assets/16_6-cover-image.png)](https://about.gitlab.com/releases/2023/11/16/gitlab-16-6-released/)

<!-- footer: "" -->
---
<!-- header:  "" -->
<style scoped>
section {
  line-height: 1.5em;
}
</style>

# Thank you

![bg right w:100%](./assets/thankyou.png)
<!-- footer: "" -->
---
<!-- header: "" -->

<div class="white-center"><p>One more thing ...</p></div>

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Dogfooding" -->

![bg w:50% vertical](./assets/dogfood.png)

<!--
References: 
- [Dogfooding for Product Managers](https://about.gitlab.com/handbook/product/product-processes/dogfooding-for-product-mgt/)
- [Engineering Principles - Dogfooding](https://about.gitlab.com/handbook/engineering/development/principles/#dogfooding)
-->
<!-- footer: "" -->
---
<!-- header:  "This slides" -->

![bg 30%](./assets/url-security-prod.png)

<!-- footer: ##URL_SECURITY_PROD## --->
---
<!-- header:  "GitLab Project" -->

![bg 30%](./assets/url-proj.png)

<!-- footer: "##URL_PROJ##" -->
---
<!-- header:  "" -->
<style scoped>
section {
  line-height: 2.5em;
}
h1 {
  color:#e24329;
}
</style>

## Extra Slides:

# Why not Ultimate?

![bg right w:100%](./assets/thankyou.png)
<!-- footer: "" -->
---
<!-- header:  "GitLab Flow" -->

![bg w:80% vertical](./assets/why/gitlabflow.png)

<!--
References: 
- [Combine GitLab Flow and GitLab Duo for a workflow powerhouse](https://about.gitlab.com/blog/2023/07/27/gitlab-flow-duo/)
-->
<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Why not Ultimate?" -->

![bg w:80% vertical](./assets/why/whynotultimate.png)

<!--
References: 
- [Why Ultimate?](https://about.gitlab.com/pricing/ultimate/)
- [Compare features](https://about.gitlab.com/pricing/feature-comparison/)
-->
<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
---
<!-- header:  "Why not Ultimate?" -->

Without Ultimate you are at on lot of _critical features_, especially as a _larger enterprise_:

- _Planning:_ Multi-level epics, OKRs
- _Security:_ Dependency scanning, DAST, API Sec., Security Approvals, related Dashboards, Audit & Compliance reports, License approvals & compliance dashboard, Policy Management
- _DORA4 metrics_

**References:** 
![w:20 h:20](./assets/gitlab-logo.svg)  [Why Ultimate?](https://about.gitlab.com/pricing/ultimate/)
![w:20 h:20](./assets/gitlab-logo.svg)  [Compare features](https://about.gitlab.com/pricing/feature-comparison/)

<!-- footer: ![w:36 h:36](./assets/gitlab-logo.svg) -->
